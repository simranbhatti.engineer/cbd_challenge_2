from flask import Flask
from flask_session import Session
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = os.getenv('SESSION_PAEMANENT')
app.config["SESSION_TYPE"] = os.getenv('SESSION_TYPE')
Session(app)

# Dummy user and product data
users = {"user1": "password1", "user2":"password2"}

products = {
    1: {"name": "Laptop", "price": 1000},
    2: {"name": "Mouse", "price": 50},
    3: {"name": "Keyboard", "price": 100}
}

users_dict = os.getenv('USERS_DICT')
from app import routes
